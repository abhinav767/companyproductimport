Key Points

1. All the files need provided by the source to be placed under feed-products folder present at the root of the project. 
2. There are two commands supported by the script, named 'import' and 'exit'. The two companies supported by the script are capterra and softwareadvice(can extend further).
3. The format for the command is: {Command_name} {company_name} {physical_location or url}.
4. Exe will be provided to run the code in the folder 'bin/Debug/net5.0'.

The framework used for developing the script is .net 5 and xUnit and Moq and for deserealizing the files provided Newtonsoft and YamlDotnet are being used.
It was not my first time writing the test cases for a project, have already implemented the same in my previous organisation using xUnit and Moq.
    