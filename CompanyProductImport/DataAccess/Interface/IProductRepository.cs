﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CompanyProductImport.DataAccess
{
    public interface IProductRepository
    {
        void SaveUpdateProducts(List<Models.Product> products);
    }
}
