﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CompanyProductImport.Models
{
    public class Product
    {        
        public List<string> categories { get; set; }

        public string tags { get; set; }

        [JsonProperty(PropertyName = "twitter", NullValueHandling = NullValueHandling.Ignore)]
        public string twitter { get; set; }

        [JsonProperty(PropertyName = "title", NullValueHandling = NullValueHandling.Ignore)]
        public string name { get; set; }
    }

    public class Root
    {
        public List<Product> products { get; set; }
    }
}
