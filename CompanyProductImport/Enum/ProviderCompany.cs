﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.ComponentModel;
using System.Threading.Tasks;

namespace CompanyProductImport.Enum
{
    public enum ProviderCompany
    {
        [Description("capterra")]
        capterra,

        [Description("softwareadvice")]
        softwareadvice
    }
}
