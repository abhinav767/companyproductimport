﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CompanyProductImport.Interface
{
    public interface ICompany
    {
        void UpdateProductInventory(string location);
    }
}
