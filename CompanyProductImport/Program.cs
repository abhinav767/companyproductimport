﻿using CompanyProductImport.Enum;
using System;
using Unity;

namespace CompanyProductImport
{
    class Program
    {
        static void Main(string[] args)
        {
            var unity = RegisterUnity();
        retry:
            Console.WriteLine(" ");
            Console.Write("$");
            var command = Console.ReadLine();
            if (!string.IsNullOrEmpty(command))
            {
                if (!command.Equals("exit", StringComparison.OrdinalIgnoreCase))
                {
                    var commandarray = command.Trim().Split(' ');
                    if (commandarray.Length == 3)
                    {
                        if (!commandarray[0].Equals("import", StringComparison.OrdinalIgnoreCase))
                        {
                            Console.WriteLine($"Cannot recognise the command {commandarray[0]}");
                            goto retry;
                        }
                        if (!string.IsNullOrEmpty(commandarray[1]) && !string.IsNullOrEmpty(commandarray[2]))
                        {
                            ProviderCompany company;
                            if (System.Enum.TryParse<ProviderCompany>(commandarray[1], out company))
                            {

                                Interface.ICompanyFactory companyFactory = unity.Resolve<Interface.ICompanyFactory>();
                                try
                                {
                                    var companyInstance = companyFactory.GetCompany(company);
                                    companyInstance.UpdateProductInventory(commandarray[2]);
                                }
                                catch (Exception ex)
                                {
                                    Console.WriteLine(ex.Message);
                                    goto retry;
                                }
                            }
                            else
                            {
                                Console.WriteLine($"Cannot recognise the Company {commandarray[1]}");
                                goto retry;
                            }
                        }
                        goto retry;
                    }
                    else
                    {
                        Console.WriteLine("Not a recognised format \n ({Command_name} {company_name} {physical_location or url})");
                        goto retry;
                    }
                }
            }
        }
        static UnityContainer RegisterUnity()
        {
            Unity.UnityContainer unityContainer = new Unity.UnityContainer();
            //unityContainer.RegisterType<Interface.ICompany, SoftwareAdvice>();
            //unityContainer.RegisterType<Interface.ICompany, Capterra>();
            unityContainer.RegisterType<DataAccess.IProductRepository, DataAccess.ProductRepository>();
            unityContainer.RegisterType<Interface.ICompanyFactory, CompanyFactory>();
            return unityContainer;
        }

    }
}
