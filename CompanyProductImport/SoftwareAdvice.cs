﻿using CompanyProductImport.DataAccess;
using CompanyProductImport.Models;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CompanyProductImport
{
    public class SoftwareAdvice : Interface.ICompany
    {
        private readonly IProductRepository _productRepository;

        public SoftwareAdvice(IProductRepository productRepository)
        {
            _productRepository = productRepository;
        }
        public void UpdateProductInventory(string location)
        {
            if (!string.IsNullOrEmpty(location))
            {
                if (location.Contains("/"))
                {
                    location = location.Replace("/", "\\");
                }
                var fileLoc = Directory.GetCurrentDirectory().Replace("bin\\Debug\\net5.0", location);
                using (StreamReader r = new StreamReader(fileLoc))
                {
                    string json = r.ReadToEnd();
                    Root items = JsonConvert.DeserializeObject<Root>(json);
                    if (items != null && items.products?.Count > 0)
                    {
                        _productRepository.SaveUpdateProducts(items.products);
                        foreach (var product in items.products)
                        {
                            Console.Write($"importing: Name: \"{product.name}\"");
                            if (product.categories?.Count > 0)
                                Console.Write($"; Categories: {string.Join(", ", product.categories)}");
                            if (!string.IsNullOrEmpty(product.twitter))
                                Console.WriteLine($"; Twitter: {product.twitter}");
                        }
                    }
                }
            }
        }
    }
}
