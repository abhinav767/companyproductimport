﻿using CompanyProductImport.Interface;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Unity;

namespace CompanyProductImport
{
    public class CompanyFactory : ICompanyFactory
    {
        private readonly DataAccess.IProductRepository _productRepository;

        [InjectionConstructor]
        public CompanyFactory(DataAccess.IProductRepository productRepository)
        {
            _productRepository = productRepository;
        }
        public ICompany GetCompany(Enum.ProviderCompany providerCompany)
        {
            switch (providerCompany)
            {
                case Enum.ProviderCompany.capterra:
                    return new Capterra(_productRepository);
                case Enum.ProviderCompany.softwareadvice:
                    return new SoftwareAdvice(_productRepository);
                default:
                    throw new Exception(string.Format("Company '{0}' does not exist", providerCompany));
            }
        }
    }
}
