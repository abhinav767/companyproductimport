﻿using CompanyProductImport.DataAccess;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using YamlDotNet.Serialization;
using YamlDotNet.Serialization.NamingConventions;

namespace CompanyProductImport
{
    public class Capterra : Interface.ICompany
    {
        private readonly IProductRepository _productRepository;
        //[InjectionConstructor]
        public Capterra(IProductRepository productRepository)
        {
            _productRepository = productRepository;
        }
        public void UpdateProductInventory(string location)
        {
            if (!string.IsNullOrEmpty(location))
            {
                if (location.Contains("/"))
                {
                    location = location.Replace("/", "\\");
                }
                var fileLoc = Directory.GetCurrentDirectory().Replace("bin\\Debug\\net5.0", location);
                using (StreamReader r = new StreamReader(fileLoc))
                {
                    string yaml = r.ReadToEnd();
                    var deserializer = new DeserializerBuilder().WithNamingConvention(CamelCaseNamingConvention.Instance).Build();
                    var items = deserializer.Deserialize<List<Models.Product>>(yaml);
                    if (items?.Count > 0)
                    {
                        _productRepository.SaveUpdateProducts(items);
                        foreach (var product in items)
                        {
                            Console.Write($"importing: Name: \"{product.name}\"");
                            if (!string.IsNullOrEmpty(product.tags))
                                Console.Write($"; Categories: {product.tags}");
                            if (!string.IsNullOrEmpty(product.twitter))
                                Console.WriteLine($"; Twitter: {product.twitter}");
                        }
                    }
                }
            }
        }
    }
}
