﻿using Moq;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Xunit;

namespace CompanyProductImport.Test
{
    public class CapterraTest
    {
        private readonly Mock<DataAccess.IProductRepository> _mockProductRepository;
        public CapterraTest()
        {
            _mockProductRepository = new Mock<DataAccess.IProductRepository>();
            _mockProductRepository.Setup(x => x.SaveUpdateProducts(It.IsAny<List<Models.Product>>()));
        }
        [Theory]
        [InlineData("feed-products")]
        public void UpdateProductInventory_WrongLocation_Throwsexception(string location)
        {
            var instance = new Capterra(_mockProductRepository.Object);

            Assert.Throws<System.IO.FileNotFoundException>(() => instance.UpdateProductInventory(location));

        }
    }
}
