﻿using Moq;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Xunit;

namespace CompanyProductImport.Test
{
    public class CompanyFactoryTest
    {
        private readonly Mock<DataAccess.IProductRepository> _mockProductRepository;
        public CompanyFactoryTest()
        {
            _mockProductRepository = new Mock<DataAccess.IProductRepository>();            
        }
        [Theory]
        [InlineData(Enum.ProviderCompany.capterra)]
        [InlineData(Enum.ProviderCompany.softwareadvice)]
        public void UpdateProductInventory_WrongLocation_Throwsexception(Enum.ProviderCompany providerCompany)
        {
            var instance = new CompanyFactory(_mockProductRepository.Object);
            var result = instance.GetCompany(providerCompany);
            if(Enum.ProviderCompany.capterra==providerCompany)            
            {
                Assert.True(result is Capterra);

            }
            else
            {
                Assert.True(result is SoftwareAdvice);
            }            
        }
    }
}
